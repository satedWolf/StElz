﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/*
 Cette option permet l'ajout d'un item a un cleint pour plusieurs jours simultanement
 Elle facilite l'ajout et l'insertion initiale des données.     
*/

namespace StElz
{
    public partial class DataEntry : Form
    {
        public DataEntry()
        {
            InitializeComponent();
        }

        private void DataEntry_Load(object sender, EventArgs e)
        { 
            //Declaration de la variable de base de donnee
            var db = new DB();
            var constante = new Const();

            //Population des clients dans le dropdown
            var clients = db.GetClients();
            ComboClient.AutoCompleteMode = AutoCompleteMode.Suggest;
            ComboClient.AutoCompleteSource = AutoCompleteSource.ListItems;
            ComboClient.Items.AddRange(clients.Select(x =>    x.Nom + " " + x.Prenom + "-" + x.Code).ToArray());

            //Population de la liste avec les jours de la semaine.
            ListJours.Items.AddRange(constante.jours);
        }

        private void Ajouter_Click(object sender, EventArgs e)
        {
            var form = new AddItem();
            var jours =  ListJours.CheckedIndices;
            var lJours = new List<int>();
            foreach (var item in jours)
            {
                lJours.Add(int.Parse(item.ToString()));
            }
            try
            {
                form.clientId = int.Parse(ComboClient.SelectedItem.ToString().Split('-')[1]);
                form.jours = lJours;
                form.Show();
            }
            catch (Exception) {
                MessageBox.Show("Une erreur est survenu !");
            }
         
        }

        private void Quitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var form = new DeleteItem();
            form.CodeClient = ComboClient.Text.Split('-')[1].ToString();
            var jours = ListJours.CheckedIndices;
            foreach (var item in jours)
            {
                int joursId = int.Parse(item.ToString());
                form.ListeJour.Add(joursId);
            }
            form.Show();



        }
    }
}
