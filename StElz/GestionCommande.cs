﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StElz
{
    public partial class GestionCommande : Form
    {
        public int clientId = 0;
        public int jourId = 0;
        public GestionCommande()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void GestionCommande_Load(object sender, EventArgs e)
        {
            var db = new DB();
            var constante = new Const();

            cmbItems.AutoCompleteMode = AutoCompleteMode.Suggest;
            cmbItems.AutoCompleteSource = AutoCompleteSource.ListItems;
            cmbClient.AutoCompleteMode = AutoCompleteMode.Suggest;
            cmbClient.AutoCompleteSource = AutoCompleteSource.ListItems;


            cmbClient.Items.AddRange(db.GetClients().Select(x => x.Prenom + " " + x.Nom + "-" + x.Code.ToString()).ToArray());
            cmbDay.Items.AddRange(constante.jours);
            cmbDayDette.Items.AddRange(constante.jours);
            cmbItems.Items.AddRange(db.GetItems().OrderBy(x => x.Nom).Select(x => x.Nom + "-" + x.Prix).ToArray());
            //Read only de l'affichage       
            txtNbr.Enabled = false;
            txtTotal.Enabled = false;
            chkCinqJourSupp.Enabled = false;

            if (DateTime.Today.DayOfWeek.ToString() == "Monday")
            {
                cmbDay.SelectedItem = "Lundi";
            }else if (DateTime.Today.DayOfWeek.ToString() == "Tuesday")
            {
                cmbDay.SelectedItem = "Mardi";
            }else if (DateTime.Today.DayOfWeek.ToString() == "Wednesday")
            {
                cmbDay.SelectedItem = "Mercredi";
            }else if (DateTime.Today.DayOfWeek.ToString() == "Thursday")
            {
                cmbDay.SelectedItem = "Jeudi";
            }else
            {
                cmbDay.SelectedItem = "Vendredi";
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DataDette.Rows.Clear();
            try { 
            var db = new DB();
            var constante = new Const();
            dataGrid.Rows.Clear();
            jourId = constante.jours.ToList().IndexOf(cmbDay.SelectedItem.ToString());
            clientId = Int32.Parse(cmbClient.SelectedItem.ToString().Split('-')[1]);
            var commande = db.GetBonCommande().Where(x => x.Client_id == clientId && x.Jour == jourId).ToList();
            var usedItem = commande.Select(x => x.Item_id).ToArray();
            var items = db.GetItems().Where(x => usedItem.Contains(x.Code)).ToList().OrderBy(x => x.Nom);
            var nbrArticles = 0;
                
            foreach (var item in items)
            {
               var article = commande.Where(x => x.Item_id == item.Code).Single();
                dataGrid.Rows.Add(item.Code + "-" + item.Nom, item.Prix, article.Quantite, article.Id);
                nbrArticles += article.Quantite;
            }

            txtNbr.Text = nbrArticles.ToString();

            txtTotal.Text = db.getTotalCommandePourJour(jourId, clientId).ToString();
            calculerNBRITEM();
            calculerPrixTotal();
            }
            catch (Exception) { }
            var db2 = new DB();
            var client = db2.GetClients().Where(x => x.Code == clientId).Single();
            lblNom.Text = client.Nom + " " + client.Prenom;
            lblTel.Text = client.tel;
            lblHeure.Text = client.heure;
         }
        public static int quantite = 0;
        private void button2_Click(object sender, EventArgs e)
        {
            if (chkCinqJoursAjouter.Checked)
            {
                ajouterItemCinqJours();
            }
            else
            {
                ajouterItem();
            }
       
        }

        public void calculerPrixTotal()
        {
            double prix = 0;
            //tocheck
            for (int i = 0; i < dataGrid.Rows.Count - 1; i++)
            {
                var row = dataGrid.Rows[i];
                var produit = double.Parse(row.Cells[1].Value.ToString()) * Int32.Parse(row.Cells[2].Value.ToString());
                prix += produit;
            }
            for (int i = 0; i < DataDette.Rows.Count - 1; i++)
            {
                var row = DataDette.Rows[i];
                var produit = double.Parse(row.Cells[1].Value.ToString());
                prix += produit;
            }
                txtTotal.Text = prix.ToString();
        }
        public void calculerNBRITEM()
        {
            int prod = 0;

            //tocheck
            for (int i = 0; i < dataGrid.Rows.Count - 1; i++)
            {
                var row = dataGrid.Rows[i];
                prod += Int32.Parse(row.Cells[2].Value.ToString());

            }
            txtNbr.Text = prod.ToString();
        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            var Commandes = new List<Commande>();
            var db = new DB();
            var commandeInitiale = db.GetBonCommandeClient(clientId);



            for (int i = 0; i < dataGrid.Rows.Count - 1; i++)
            {
                var com = new Commande();
                var row = dataGrid.Rows[i];
                com.Id = Int32.Parse(row.Cells[3].Value.ToString());
                com.Quantite = Int32.Parse(row.Cells[2].Value.ToString());
                com.Item_id = Int32.Parse(row.Cells[0].Value.ToString().Split('-')[0]);
                com.Somme_Partielle = com.Quantite * double.Parse(row.Cells[1].Value.ToString());
                com.Client_id = clientId;
                com.Jour = jourId;
                Commandes.Add(com);
                if (commandeInitiale.Where(x => x.Id == com.Id && com.Jour == x.Jour).Any())
                {
                    commandeInitiale.Where(x => x.Id == com.Id && com.Jour == x.Jour).Single().Quantite = com.Quantite;
                }
                else
                { 
                    db.InsertCommande(com);
                }
                
            }
            
            db.updateCommande(commandeInitiale);

            MessageBox.Show("Enregistrement terminé");


        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            try{ 
            var idCommande = Int32.Parse(dataGrid.SelectedRows[0].Cells[3].Value.ToString());
            var db = new DB();
            dataGrid.Rows.RemoveAt(dataGrid.SelectedRows[0].Index);
            if (checkRetirer.Checked)
            {
                    if (chkCinqJourSupp.Checked)
                    {
                        var commande = db.GetBonCommande().Where(x => x.Id == idCommande).First();
                        var commandes = db.GetBonCommande().Where(x => x.Item_id == commande.Item_id && x.Client_id == commande.Client_id).ToList();

                        foreach(var com in commandes)
                        {
                            db.SupprimerCommande(com.Id);
                        }
                        chkCinqJourSupp.Checked = false;
                        chkCinqJourSupp.Enabled = false;
                    }
                    else
                    {
                        db.SupprimerCommande(idCommande);
                    }
                    checkRetirer.Checked = false;
            }
            }
            catch (Exception)
            {

            }

        }

      
        private void btnCalculer_Click(object sender, EventArgs e)
        {
            calculerPrixTotal();
            calculerNBRITEM();
        }

        private void chkExpress_CheckedChanged(object sender, EventArgs e)
        {
          
                cmbClient.Enabled = true;
                cmbDay.Enabled = true;
                btnEnregistrer.Enabled = true;
                btnRechercher.Enabled = true;
                btnAjouterDette.Enabled = true;
                cmbDayDette.Enabled = true;
             
           
        }

        private void btnImprimer_Click(object sender, EventArgs e)
        {
            calculerNBRITEM();
            calculerPrixTotal();
            Rapport rapport = new Rapport();
            var db = new DB();
            var listArticle = new List<ArticleFacture>();
            var listDette = new List<DetteFacture>();
            for (int i = 0; i < dataGrid.Rows.Count - 1; i++)
            {
                var row = dataGrid.Rows[i];
                var temp = new ArticleFacture();
                temp.nom = row.Cells[0].Value.ToString();
                temp.Prix = Decimal.Parse( row.Cells[1].Value.ToString());
                temp.Quantite = Int32.Parse(row.Cells[2].Value.ToString());
                listArticle.Add(temp);
            }
            for (int i = 0; i < DataDette.Rows.Count - 1; i++)
            {
                var row = DataDette.Rows[i];
                var temp = new DetteFacture();
                temp.jour = row.Cells[0].Value.ToString();
                temp.Prix = double.Parse(row.Cells[1].Value.ToString());
                
                listDette.Add(temp);
            }
          
                rapport.CreateFacture(listArticle, cmbClient.SelectedItem.ToString(), txtNbr.Text.ToString(), txtTotal.Text.ToString(), cmbDay.SelectedItem.ToString(), dateImpression.Value.ToShortDateString(), listDette);

            

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
            this.AcceptButton = btnRechercher;
        }

        private void btnAjouterDette_Click(object sender, EventArgs e)
        {
        }

        private void btnAjouterDette_Click_1(object sender, EventArgs e)
        {

            var db = new DB();
            var constante = new Const();
            try
            {
                var dept = db.GetDept().Where(x => x.Id_client == int.Parse(cmbClient.SelectedItem.ToString().Split('-')[1]) && x.Id_jour == constante.jours.ToList().IndexOf(cmbDayDette.SelectedItem.ToString())).OrderByDescending(x => x.ID).First();
                DataDette.Rows.Add(cmbDayDette.SelectedItem.ToString(), dept.Total);
            }
            catch (Exception)
            {
                MessageBox.Show("Aucune dette d'enregistrée");
            }
                calculerPrixTotal();
        }

        private void cmbItems_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        public void ajouterItem()
        {
            var db = new DB();
            var item = db.GetItems().Where(x => x.Nom == cmbItems.SelectedItem.ToString().Split('-')[0] && x.Prix == decimal.Parse(cmbItems.SelectedItem.ToString().Split('-')[1])).First();
            var commande = new Commande();
            commande.Client_id = clientId;
            commande.Item_id = item.Code;
            commande.Jour = jourId;
            commande.Quantite = 0;
            commande.Somme_Partielle = 0;
            // db.InsertCommande(commande);
            var id = 0;
            if (db.GetBonCommande().Any())
            {
                id = db.GetBonCommande().Select(x => x.Id).Max();
            }
            else
            {
                id = 1;
            }

            bool trouver = false;
            for(int i = 0; i < dataGrid.RowCount; i++)
            {
                if(dataGrid.Rows[i].Cells[0].Value != null && dataGrid.Rows[i].Cells[0].Value.ToString() == item.Code + "-" + item.Nom)
                {
                    dataGrid.Rows[i].Selected = true;
                    trouver = true;
                    break;
                }
            }
            if (!trouver)
            {
                dataGrid.Rows.Add(item.Code + "-" + item.Nom, item.Prix, 0, id);
            }


            calculerPrixTotal();
            calculerNBRITEM();
            cmbItems.SelectedIndex = -1;
        }
        public void ajouterItemCinqJours()
        {
            var db = new DB();
            var item = db.GetItems().Where(x => x.Nom == cmbItems.SelectedItem.ToString().Split('-')[0] && x.Prix == decimal.Parse(cmbItems.SelectedItem.ToString().Split('-')[1])).First();
            var commande = new Commande();
            commande.Client_id = clientId;
            commande.Item_id = item.Code;
            commande.Jour = jourId;
            commande.Quantite = 0;
            commande.Somme_Partielle = 0;
            
            
            
            var id = 0;
            if (db.GetBonCommande().Any())
            {
                id = db.GetBonCommande().Select(x => x.Id).Max();
            }
            else
            {
                id = 1;
            }

            bool trouver = false;
            for (int i = 0; i < dataGrid.RowCount; i++)
            {
                if (dataGrid.Rows[i].Cells[0].Value != null && dataGrid.Rows[i].Cells[0].Value.ToString() == item.Code + "-" + item.Nom)
                {
                    dataGrid.Rows[i].Selected = true;
                    trouver = true;
                    break;
                }
            }
            if (!trouver)
            {

                for(int i =0; i < 5; i++)
                {
                    var commandeExiste = db.GetBonCommandeClient(clientId).Where(x => x.Item_id == item.Code && x.Jour == i).SingleOrDefault();

                    if(commandeExiste != null)
                    {
                        commandeExiste.Quantite += quantite;
                        List<Commande> list = new List<Commande>();
                        list.Add(commandeExiste);
                        db.updateCommande(list);
                    }
                    else
                    {
                        commande.Jour = i;
                        commande.Quantite = quantite;
                        db.InsertCommande(commande);
                    }   
                }
                dataGrid.Rows.Add(item.Code + "-" + item.Nom, item.Prix, 0, id);
                
            }


            calculerPrixTotal();
            calculerNBRITEM();
            cmbItems.SelectedIndex = -1;
        }
        private void btnPayLater_Click(object sender, EventArgs e)
        {
            //Update du prix total
            calculerPrixTotal();

            var db = new DB();
            var dept = new Dept();
            try
            {
                dept.Id_client = int.Parse(cmbClient.SelectedItem.ToString().Split('-')[1]);
                dept.Id_jour = jourId;
                dept.Total = double.Parse(txtTotal.Text);
                db.InsertDept(dept);
                MessageBox.Show("Dette enregistrer !");
            }
            catch (Exception)
            {
                MessageBox.Show("Une erreur est survenu");
            }
          

        }

        private void cmbClient_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkRetirer_CheckedChanged(object sender, EventArgs e)
        {
            chkCinqJourSupp.Enabled = true;
        }
    }
}
