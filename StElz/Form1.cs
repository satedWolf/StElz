﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StElz
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnRetirerItem_Click(object sender, EventArgs e)
        {
            RetirerItem form = new RetirerItem();
            form.Show();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            AjouterClient form = new AjouterClient();
            form.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            AjouterItem form = new AjouterItem();
            form.Show();
        }

        private void btnRetirerClient_Click(object sender, EventArgs e)
        {
            RetirerClient form = new RetirerClient();
            form.Show();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //this.Width = Screen.PrimaryScreen.Bounds.Width;
            this.StartPosition = FormStartPosition.Manual;
            this.Location = new Point(0, 0);
        }

        private void btnGestionCommande_Click(object sender, EventArgs e)
        {
            GestionCommande form = new GestionCommande();
            form.Show();
        }

        private void btnCommandeDuJour_Click(object sender, EventArgs e)
        {
            CommandeDuJour form = new CommandeDuJour();
            form.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDataEntry_Click(object sender, EventArgs e)
        {
            var form = new DataEntry();
            form.Show();
        }
    }
}
