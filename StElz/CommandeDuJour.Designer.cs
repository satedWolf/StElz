﻿namespace StElz
{
    partial class CommandeDuJour
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGenerer = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.listItems = new System.Windows.Forms.CheckedListBox();
            this.ListJours = new System.Windows.Forms.CheckedListBox();
            this.SuspendLayout();
            // 
            // btnGenerer
            // 
            this.btnGenerer.Location = new System.Drawing.Point(56, 414);
            this.btnGenerer.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnGenerer.Name = "btnGenerer";
            this.btnGenerer.Size = new System.Drawing.Size(91, 27);
            this.btnGenerer.TabIndex = 1;
            this.btnGenerer.Text = "Genérer";
            this.btnGenerer.UseVisualStyleBackColor = true;
            this.btnGenerer.Click += new System.EventHandler(this.btnGenerer_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 17);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Jours";
            // 
            // listItems
            // 
            this.listItems.FormattingEnabled = true;
            this.listItems.Location = new System.Drawing.Point(9, 118);
            this.listItems.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.listItems.Name = "listItems";
            this.listItems.Size = new System.Drawing.Size(195, 289);
            this.listItems.TabIndex = 3;
            // 
            // ListJours
            // 
            this.ListJours.FormattingEnabled = true;
            this.ListJours.Location = new System.Drawing.Point(9, 32);
            this.ListJours.Margin = new System.Windows.Forms.Padding(2);
            this.ListJours.Name = "ListJours";
            this.ListJours.Size = new System.Drawing.Size(195, 79);
            this.ListJours.TabIndex = 4;
            // 
            // CommandeDuJour
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(212, 450);
            this.Controls.Add(this.ListJours);
            this.Controls.Add(this.listItems);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnGenerer);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "CommandeDuJour";
            this.Text = "CommandeDuJour";
            this.Load += new System.EventHandler(this.CommandeDuJour_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnGenerer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckedListBox listItems;
        private System.Windows.Forms.CheckedListBox ListJours;
    }
}