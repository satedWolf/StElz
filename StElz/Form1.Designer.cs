﻿namespace StElz
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.btnGestionCommande = new System.Windows.Forms.Button();
            this.btnCommandeDuJour = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.btnDataEntry = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(9, 10);
            this.button1.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(88, 30);
            this.button1.TabIndex = 0;
            this.button1.Text = "Gestion Clients";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(101, 10);
            this.button2.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(88, 30);
            this.button2.TabIndex = 1;
            this.button2.Text = "Gestion Items";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnGestionCommande
            // 
            this.btnGestionCommande.Location = new System.Drawing.Point(193, 10);
            this.btnGestionCommande.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnGestionCommande.Name = "btnGestionCommande";
            this.btnGestionCommande.Size = new System.Drawing.Size(113, 30);
            this.btnGestionCommande.TabIndex = 4;
            this.btnGestionCommande.Text = "Clients";
            this.btnGestionCommande.UseVisualStyleBackColor = true;
            this.btnGestionCommande.Click += new System.EventHandler(this.btnGestionCommande_Click);
            // 
            // btnCommandeDuJour
            // 
            this.btnCommandeDuJour.Location = new System.Drawing.Point(310, 10);
            this.btnCommandeDuJour.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnCommandeDuJour.Name = "btnCommandeDuJour";
            this.btnCommandeDuJour.Size = new System.Drawing.Size(113, 30);
            this.btnCommandeDuJour.TabIndex = 5;
            this.btnCommandeDuJour.Text = "Commandes du jour";
            this.btnCommandeDuJour.UseVisualStyleBackColor = true;
            this.btnCommandeDuJour.Click += new System.EventHandler(this.btnCommandeDuJour_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(768, 10);
            this.button3.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(113, 30);
            this.button3.TabIndex = 6;
            this.button3.Text = "Quitter";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // btnDataEntry
            // 
            this.btnDataEntry.Location = new System.Drawing.Point(427, 10);
            this.btnDataEntry.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.btnDataEntry.Name = "btnDataEntry";
            this.btnDataEntry.Size = new System.Drawing.Size(113, 30);
            this.btnDataEntry.TabIndex = 7;
            this.btnDataEntry.Text = "DataEntry";
            this.btnDataEntry.UseVisualStyleBackColor = true;
            this.btnDataEntry.Click += new System.EventHandler(this.btnDataEntry_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(890, 46);
            this.Controls.Add(this.btnDataEntry);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.btnCommandeDuJour);
            this.Controls.Add(this.btnGestionCommande);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "Form1";
            this.Text = " ";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button btnGestionCommande;
        private System.Windows.Forms.Button btnCommandeDuJour;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button btnDataEntry;
    }
}

