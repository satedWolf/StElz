﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StElz
{
    class Item
    {
        public int Code { get; set; }
        public String Nom { get; set; }
        public Decimal Prix { get; set; }
         
    }
}
