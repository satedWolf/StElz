﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StElz
{
    public partial class AjouterClient : Form
    {
        public AjouterClient()
        {
            InitializeComponent();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void btnEnregistrer_Click(object sender, EventArgs e)
        {
            var db = new DB();
            var client = new Client();
            client.Nom = txtNom.Text;
            client.Prenom = txtPrenom.Text;
            client.heure = txtHeure.Text;
            client.tel = txtTel.Text;
            client.Code = Int32.Parse(txtCode.Text);
            db.updateClient(client);
            clearForm();

        }

        private void btnQuitter_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void AjouterClient_Load(object sender, EventArgs e)
        {

            txtCode.Visible = false;
            var db = new DB();
            var listClients = db.GetClients().ToList();
            comboClients.AutoCompleteMode = AutoCompleteMode.Suggest;
            comboClients.AutoCompleteSource = AutoCompleteSource.ListItems;
            comboClients.Items.AddRange(listClients.Select(x => x.Code + "-" + x.Prenom + " " + x.Nom).ToArray());
            //this.AcceptButton = btnEnregistrer;
            //txtPrenom.Enabled = false;
            //txtNom.Enabled = false;
            //txtTel.Enabled = false;
            //txtHeure.Enabled = false;
        }

        private void Ajouter_Click(object sender, EventArgs e)
        {
            var db = new DB();

            var client = new Client();

            client.Nom = txtNom.Text;
            client.Prenom = txtPrenom.Text;
            client.heure = txtHeure.Text;
            client.tel = txtTel.Text;
            db.InsertClient(client);
            clearForm();



        }

        private void comboClients_SelectedIndexChanged(object sender, EventArgs e)
        {
             var id = comboClients.Text.Split('-')[0];
            var db = new DB();
            if(id == "")
            {
                Ajouter.Enabled = false;
            }
            else
            {
                var client = db.GetClients().Where(x => x.Code == Int32.Parse(id)).SingleOrDefault();
                txtCode.Text = client.Code.ToString();
                txtHeure.Text = client.heure;
                txtNom.Text = client.Nom;
                txtPrenom.Text = client.Prenom;
                txtTel.Text = client.tel;
                Ajouter.Enabled = false;
            }
      


        }

        private void btnSupprimer_Click(object sender, EventArgs e)
        {
            var db = new DB();

            var response = MessageBox.Show("Etes vous sur de vouloir supprimer " + txtNom.Text + " " + txtPrenom.Text, "Supprimer Client", MessageBoxButtons.YesNo);

            if (response == DialogResult.Yes)
            {
                db.SupprimerClient(Int32.Parse(txtCode.Text));
                this.Close();
            }
            clearForm();
        }

        public void clearForm()
        {
            txtPrenom.Text = "";
            txtNom.Text = "";
            txtHeure.Text = "";
            txtTel.Text = "";
            txtCode.Text = "";
            comboClients.SelectedIndex = -1;
            Ajouter.Enabled = true;
            var db = new DB();
            var listClients = db.GetClients().ToList();
            comboClients.Items.AddRange(listClients.Select(x => x.Code + "-" + x.Prenom + " " + x.Nom).ToArray());
        }

        private void button1_Click(object sender, EventArgs e)
        {
            clearForm();
        }
    }
}
