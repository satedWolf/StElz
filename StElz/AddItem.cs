﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StElz
{
    public partial class AddItem : Form
    {
        public AddItem()
        {
            InitializeComponent();
        }

        public int clientId;
        public List<int> jours;

        private void AddItem_Load(object sender, EventArgs e)
        {
            //declaration de la variable de base donnée
            var db = new DB();

            //Population du combo item et ajout du autocomplete
            cmbItems.AutoCompleteMode = AutoCompleteMode.Suggest;
            cmbItems.AutoCompleteSource = AutoCompleteSource.ListItems;
            cmbItems.Items.AddRange(db.GetItems().OrderBy(x => x.Code).Select(x =>   x.Nom + "-" + x.Code + "-"  + x.Prix).ToArray());
           
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var db = new DB();

            foreach (var jour in jours)
            {
                try
                {
               
                    if(db.GetBonCommandeClient(clientId).Where(x => x.Item_id == int.Parse(cmbItems.SelectedItem.ToString().Split('-')[1]) && x.Jour == jour).Any())
                    {
                        var commande = db.GetBonCommandeClient(clientId).Where(x => x.Item_id == int.Parse(cmbItems.SelectedItem.ToString().Split('-')[1]) && x.Jour == jour).Single();
                        commande.Quantite += int.Parse(textQuantite.Text);
                        List<Commande> list = new List<Commande>();
                        list.Add(commande);
                        db.updateCommande(list);
                    }
                    else
                    {
                        var commande = new Commande();
                        commande.Client_id = clientId;
                        commande.Item_id = int.Parse(cmbItems.SelectedItem.ToString().Split('-')[1]);
                        commande.Jour = jour;
                        commande.Quantite = int.Parse(textQuantite.Text);
                        db.InsertCommande(commande);
                    }
               
                }
                catch (Exception)
                {
                    MessageBox.Show("Une erreur est survenu !");
                }
            }
            cmbItems.SelectedIndex = -1;
            textQuantite.Text = "";

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
