﻿namespace StElz
{
    partial class DataEntry
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ComboClient = new System.Windows.Forms.ComboBox();
            this.ListJours = new System.Windows.Forms.CheckedListBox();
            this.Ajouter = new System.Windows.Forms.Button();
            this.Quitter = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ComboClient
            // 
            this.ComboClient.FormattingEnabled = true;
            this.ComboClient.Location = new System.Drawing.Point(9, 24);
            this.ComboClient.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ComboClient.Name = "ComboClient";
            this.ComboClient.Size = new System.Drawing.Size(372, 21);
            this.ComboClient.TabIndex = 0;
            // 
            // ListJours
            // 
            this.ListJours.FormattingEnabled = true;
            this.ListJours.Location = new System.Drawing.Point(9, 73);
            this.ListJours.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.ListJours.Name = "ListJours";
            this.ListJours.Size = new System.Drawing.Size(372, 109);
            this.ListJours.TabIndex = 1;
            // 
            // Ajouter
            // 
            this.Ajouter.Location = new System.Drawing.Point(9, 208);
            this.Ajouter.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Ajouter.Name = "Ajouter";
            this.Ajouter.Size = new System.Drawing.Size(117, 29);
            this.Ajouter.TabIndex = 2;
            this.Ajouter.Text = "Ajouter";
            this.Ajouter.UseVisualStyleBackColor = true;
            this.Ajouter.Click += new System.EventHandler(this.Ajouter_Click);
            // 
            // Quitter
            // 
            this.Quitter.Location = new System.Drawing.Point(271, 208);
            this.Quitter.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Quitter.Name = "Quitter";
            this.Quitter.Size = new System.Drawing.Size(110, 29);
            this.Quitter.TabIndex = 3;
            this.Quitter.Text = "Quitter";
            this.Quitter.UseVisualStyleBackColor = true;
            this.Quitter.Click += new System.EventHandler(this.Quitter_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 7);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(33, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Client";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 57);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Jours";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(141, 208);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(117, 29);
            this.button1.TabIndex = 6;
            this.button1.Text = "Supprimer";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // DataEntry
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(392, 246);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Quitter);
            this.Controls.Add(this.Ajouter);
            this.Controls.Add(this.ListJours);
            this.Controls.Add(this.ComboClient);
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "DataEntry";
            this.Text = "DataEntry";
            this.Load += new System.EventHandler(this.DataEntry_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox ComboClient;
        private System.Windows.Forms.CheckedListBox ListJours;
        private System.Windows.Forms.Button Ajouter;
        private System.Windows.Forms.Button Quitter;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button button1;
    }
}