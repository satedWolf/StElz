﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StElz
{
    class Rapport
    {
        public void CreateFacture(List<ArticleFacture> list, String nom, String nbr, String total, String Day, String Date, List<DetteFacture> listDette)
        {

            var db = new DB();
            var client = db.GetClients().Where(x => x.Code == int.Parse(nom.Split('-')[1])).Single();

            Document document = new Document(PageSize.LETTER, 10f, 10f, 35f, 35f);

            PdfWriter.GetInstance(document, (Stream)new FileStream("Facture\\" + nom.Split('-')[0] + "-" + Day + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf", FileMode.Create));
            PdfPTable header = new PdfPTable(5);
            Paragraph separateur = new Paragraph("\n");
            PdfPTable footer = new PdfPTable(5);
            Font fDate = new Font();
            fDate.Size = 8;
            PdfPTable table = new PdfPTable(5);

            PdfPTable tableDette = new PdfPTable(3);

            header.DefaultCell.Border = Rectangle.NO_BORDER;
            table.DefaultCell.Border = Rectangle.BOTTOM_BORDER;
            tableDette.DefaultCell.Border = Rectangle.BOTTOM_BORDER;
            footer.DefaultCell.Border = Rectangle.NO_BORDER;
            document.Open();

            //Header facture
            PdfPCell cellTitre = new PdfPCell(new Phrase("Resto Le St - Elz\n1750 Boulevard Saint - Elzéar\nLaval, QC H7L 3N2\n(450) 686 - 9553"));
            cellTitre.Colspan = 2;
            cellTitre.Border = Rectangle.NO_BORDER;
            header.AddCell(cellTitre);
            header.AddCell("");
            header.AddCell("");
            header.AddCell(new Phrase(Day + " " + Date, fDate));
            header.AddCell("");
            header.AddCell("");
            header.AddCell(client.Nom + " " + client.Prenom);
            header.AddCell("");
            header.AddCell("");
            document.Add(header);

            //Body facture
            document.Add(separateur);
            table.AddCell("Quantite");
            PdfPCell cell = new PdfPCell(new Phrase("Articles"));
            cell.Colspan = 2;
            cell.Border = Rectangle.BOTTOM_BORDER;
            table.AddCell(cell);
            table.AddCell("Prix");
            table.AddCell("Total");

            list = list.OrderBy(x => x.nom).ToList();
            foreach (var item in list)
            {
                table.AddCell(item.Quantite.ToString());
                PdfPCell cellItem = new PdfPCell(new Phrase(item.nom.Split('-')[1]));
                cellItem.Colspan = 2;
                cellItem.Border = Rectangle.BOTTOM_BORDER;
                table.AddCell(cellItem);
                table.AddCell(string.Format("{0:C}", item.Prix).Substring(1) + "$");
                table.AddCell(string.Format("{0:C}", (item.Prix * item.Quantite)).Substring(1) + "$");
            }
            document.Add(table);
            document.Add(separateur);
            document.Add(separateur);


            //Dette
            if (listDette.Count > 0)
            {
                tableDette.AddCell(" ");
                tableDette.AddCell("Jour");
                tableDette.AddCell("Total");
                foreach (var item in listDette)
                {
                    tableDette.AddCell(" ");
                    tableDette.AddCell(item.jour);
                    tableDette.AddCell(string.Format("{0:C}", item.Prix).Substring(1) + "$");
                }
                document.Add(tableDette);

            }


            //footer
            footer.AddCell(" ");
            footer.AddCell(" ");
            footer.AddCell(" ");
            footer.AddCell(" ");
            footer.AddCell(" ");
            footer.AddCell
                ("Nombre Articles : " + nbr + "\n");
            footer.AddCell("");
            footer.AddCell("");
            footer.AddCell("Total :");
            footer.AddCell("" + string.Format("{0:C}", decimal.Parse(total)).Substring(1) + "$\n");
            document.Add(footer);




            document.Close();
            Process.Start("Facture\\" + nom.Split('-')[0] + "-" + Day + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf");


        }
        public void ordreDuJour(List<int> dayId, List<String> Day, List<String> selectedItems)
        {
            var db = new DB();
            var commandes = db.GetBonCommande().ToList();
            var items = db.GetItems();
            var clients = db.GetClients();
            var cons = new Const();
            Document document = new Document(PageSize.LETTER, 10f, 10f, 35f, 35f);
            try
            {


                PdfWriter.GetInstance(document, (Stream)new FileStream("Rapport\\" + Day + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf", FileMode.Create));
            }
            catch (Exception e)
            {
                MessageBox.Show("Un rapport est deja ouvert. Veuillez le fermer et recommencer.");
            }
            PdfPTable entete = new PdfPTable(5);
            Paragraph separateur = new Paragraph();
            separateur.Add("\n");
            entete.DefaultCell.Border = Rectangle.NO_BORDER;
            document.Open();
            string StringJour = "";
            foreach (var jour in Day)
            {

                StringJour += jour + " ";


            }

            //Entete du document
            entete.AddCell(DateTime.Now.ToShortDateString());
            entete.AddCell(" ");
            entete.AddCell(StringJour);
            entete.AddCell("\n");
            entete.AddCell("\n");
            entete.AddCell("\n");
            document.Add(entete);

            //Body du document

            foreach (var ob in selectedItems)
            {
                var table = new PdfPTable(3 + Day.Count);
                var comms = commandes.Where(x => x.Item_id == int.Parse(ob.Split('-')[1])).GroupBy(x => x.Client_id);
                var TitreItem = new PdfPCell(new Phrase(ob.Split('-')[0]));
                var cumulatif = 0;
                TitreItem.Colspan = 2;
                TitreItem.HorizontalAlignment = 1;
                table.AddCell(TitreItem);
                if (Day.Contains("Lundi"))
                {
                    table.AddCell("Lundi");
                }

                if (Day.Contains("Mardi"))
                {
                    table.AddCell("Mardi");
                }


                if (Day.Contains("Mercredi"))
                {
                    table.AddCell("Mercredi");
                }

                if (Day.Contains("Jeudi"))
                {
                    table.AddCell("Jeudi");
                }
                if (Day.Contains("Vendredi"))
                {
                    table.AddCell("Vendredi");
                }
                table.AddCell("");

                foreach (var it in comms)
                {
                    table.AddCell(clients.Where(x => x.Code == it.Key).Select(x => x.Nom + " " + x.Prenom).Single());
                    table.AddCell(ob.Split('-')[0]);
                    for (int i = 0; i < Day.Count; i++)
                    {
                        if (dayId.Contains(dayId[i]))
                        {

                            if (it.Where(x => x.Jour == dayId[i]).Any())
                            {
                                //var test = it.Where(x => x.Jour == i).Select(x => x.Quantite).Single();
                                cumulatif += it.Where(x => x.Jour == dayId[i]).Select(x => x.Quantite).Single();
                                table.AddCell(it.Where(x => x.Jour == dayId[i]).Select(x => x.Quantite).Single().ToString());
                            }
                            else
                            {
                                table.AddCell(" ");
                            }
                        }
                        else
                        {
                            table.AddCell(" ");

                        }

                    }

                    table.AddCell(" ");
                }
                for(int i = 0; i < table.NumberOfColumns-1; i++)
                {
                    table.AddCell(" ");
                }
                table.AddCell("Total : " + cumulatif);
                document.Add(separateur);
                document.Add(table);

            }

            document.Close();
            Process.Start("Rapport\\" + Day + "-" + DateTime.Now.ToString("yyyy-MM-dd") + ".pdf");
        }
    }
}
