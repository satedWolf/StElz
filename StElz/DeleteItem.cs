﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StElz
{
    public partial class DeleteItem : Form
    {

        public String CodeClient { get; set; }
        public List<int> ListeJour = new List<int>(); 
        public DeleteItem()
        {
            InitializeComponent();
        }

        private void DeleteItem_Load(object sender, EventArgs e)
        {
            var db = new DB();
            var commandes = db.GetBonCommandeClient(int.Parse(CodeClient));
            var idItems = commandes.Select(x => x.Item_id).ToArray();
            var items = db.GetItems().Where(x => idItems.Contains(x.Code));
            cmbItems.Items.AddRange(items.Select(x => x.Nom + "-" + x.Code).ToArray());
           
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var db = new DB();
            var commandes = db.GetBonCommandeClient(int.Parse(CodeClient));
            var codeItem = cmbItems.SelectedItem.ToString().Split('-')[1];

            foreach(var jour in ListeJour)
            {
                try
                {
                    var commande = commandes.Where(x => x.Item_id == int.Parse(codeItem) && x.Jour == jour).SingleOrDefault();
                    db.SupprimerCommande(commande.Id);

                }
                catch (Exception)
                {

                }
            }

            cmbItems.SelectedIndex = -1;


        }
    }
}
