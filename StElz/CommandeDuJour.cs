﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StElz
{
    public partial class CommandeDuJour : Form
    {
        public CommandeDuJour()
        {
            InitializeComponent();
        }

        private void CommandeDuJour_Load(object sender, EventArgs e)
        {
            this.AcceptButton = btnGenerer;
            var constante = new Const();
            var db = new DB();
 
            ListJours.Items.AddRange(constante.jours);
            listItems.Items.AddRange(db.GetItems().OrderBy(x => x.Nom).Select(x => x.Nom + "-"+ x.Code).ToArray());
            if (DateTime.Today.DayOfWeek.ToString() == "Monday")
            {
                ListJours.SelectedItem = "Lundi";
                 
            }
            else if (DateTime.Today.DayOfWeek.ToString() == "Tuesday")
            {
                ListJours.SelectedItem = "Mardi";
            }
            else if (DateTime.Today.DayOfWeek.ToString() == "Wednesday")
            {
                ListJours.SelectedItem = "Mercredi";
            }
            else if (DateTime.Today.DayOfWeek.ToString() == "Thursday")
            {
                ListJours.SelectedItem = "Jeudi";
            }
            else
            {
                ListJours.SelectedItem = "Vendredi";
            }

        }

        private void btnGenerer_Click(object sender, EventArgs e)
        {
            var rapport = new Rapport();
            var cons = new Const();


            var joursSelectionne = ListJours.CheckedItems;
            var jours = new List<int>();
            var Nomjours = new List<String>();
            foreach (var item in joursSelectionne)
            {
              var val =  Array.IndexOf(cons.jours,item.ToString());
                jours.Add(val);
                Nomjours.Add(item.ToString());
            }


            var items = new List<String>();
            foreach(var item in listItems.CheckedItems)
            {
                items.Add(item.ToString());
            }
          rapport.ordreDuJour(jours, Nomjours, items);
        }
    }
}
