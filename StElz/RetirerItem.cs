﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StElz
{
    public partial class RetirerItem : Form
    {
        public RetirerItem()
        {
            InitializeComponent();
        }

        private void RetirerItem_Load(object sender, EventArgs e)
        {
            
            var db = new DB();
            var items = db.GetItems();
            cmbItems.AutoCompleteMode = AutoCompleteMode.Suggest;
            cmbItems.AutoCompleteSource = AutoCompleteSource.ListItems;
         
            cmbItems.Items.AddRange(items.Select(x =>  x.Nom+"-"+x.Prix).ToArray());
        }

        private void btnSupp_Click(object sender, EventArgs e)
        {
            var db = new DB();

            var response = MessageBox.Show("Etes vous sur de vouloir supprimer " + cmbItems.SelectedItem.ToString(), "Supprimer Client", MessageBoxButtons.YesNo);

            if (response == DialogResult.Yes)
            {
               // db.SupprimerItem(cmbItems.SelectedItem.ToString());
                this.Close();
            }
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
