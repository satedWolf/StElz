﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StElz
{
    public partial class RetirerClient : Form
    {
        public RetirerClient()
        {
            InitializeComponent();
        }

        private void RetirerClient_Load(object sender, EventArgs e)
        {
            var db = new DB();
            var clients = db.GetClients();
            cmbClient.Items.AddRange(clients.Select(x => x.Code + "-" + x.Nom + "," + x.Prenom).ToArray());
        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSupp_Click(object sender, EventArgs e)
        {
            var db = new DB();

            var response = MessageBox.Show("Etes vous sur de vouloir supprimer " + cmbClient.SelectedItem.ToString(), "Supprimer Client", MessageBoxButtons.YesNo);

            if (response == DialogResult.Yes)
            {
                db.SupprimerClient(Int32.Parse(cmbClient.SelectedItem.ToString().Split('-')[0]));
                this.Close();
            }
           
        }
    }
}
