﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StElz
{
    public partial class AjouterItem : Form
    {
        public AjouterItem()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {


            var nom = cmbItems.Text.Split('-')[0];
            var db = new DB();
            var item = db.GetItems().Where(x => x.Nom == nom).SingleOrDefault();
            
                item.Nom = txtNom.Text;
                item.Prix = Decimal.Parse(txtPrix.Text);
                db.updateItem(item);
                clearForm();
           
            
        }

        private void AjouterItem_Load(object sender, EventArgs e)
        {
            var db = new DB();
            var items = db.GetItems();
            cmbItems.AutoCompleteMode = AutoCompleteMode.Suggest;
            cmbItems.AutoCompleteSource = AutoCompleteSource.ListItems;
            cmbItems.Items.AddRange(items.Select(x => x.Nom + "-" + x.Prix).ToArray());
            txtCode.Visible = false;

        }

        private void cmbItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            var nom = cmbItems.Text.Split('-')[0];
            var db = new DB();
            var item = db.GetItems().Where(x => x.Nom == nom).SingleOrDefault();
            txtNom.Text = item.Nom;
            txtPrix.Text = item.Prix.ToString();
            txtCode.Text = item.Code.ToString();
            btnAjouter.Enabled = false;

        }

        private void btnAjouter_Click(object sender, EventArgs e)
        {
            var db = new DB();
            var item = new Item();
            item.Nom = txtNom.Text;
            item.Prix = Decimal.Parse(txtPrix.Text);
            //item.Periode = rdDesjeuner.Checked ? 1 : 2;
            db.InsertItem(item);
            clearForm();
        }
        public void clearForm()
        {
            txtCode.Text = "";
            txtNom.Text = "";
            txtPrix.Text = "";
            btnAjouter.Enabled = true;
            cmbItems.Items.Clear();
            var db = new DB();
            var items = db.GetItems();
            cmbItems.Items.AddRange(items.Select(x => x.Nom + "-" + x.Prix).ToArray());
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            var db = new DB();

            var response = MessageBox.Show("Etes vous sur de vouloir supprimer " + txtNom.Text.ToString(), "Supprimer Item", MessageBoxButtons.YesNo);

            if (response == DialogResult.Yes)
            {
                db.SupprimerItem(int.Parse(txtCode.Text));
                clearForm();
            }
        }
    }
}
