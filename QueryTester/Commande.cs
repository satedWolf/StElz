﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StElz
{
    class Commande
    {
        public int Id { get; set; }
        public int Jour { get; set; }
        public int Client_id { get; set; }
        public int Item_id { get; set; }
        public int Quantite { get; set; }
        public double Somme_Partielle { get; set; }
    }

     
}
