﻿using System;
using System.Collections.Generic;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StElz
{
    class DB
    {
        public List<Client> GetClients()
        {
            var list = new List<Client>();
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM Clients"))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (OleDbDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            var tempClient = new Client();
                            tempClient.Code = Int32.Parse(sdr["ID"].ToString());
                            tempClient.Nom = sdr["Nom"].ToString();
                            tempClient.Prenom = sdr["Prenom"].ToString();
                            tempClient.tel = sdr["Tel"].ToString();
                            tempClient.heure = sdr["Heure"].ToString();
                            list.Add(tempClient);
                        }
                    }
                    con.Close();
                }
            }
            return list;

        }
         
        public List<Dept> GetDept()
        {
            var list = new List<Dept>();
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM Dept"))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (OleDbDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            var temp = new Dept();
                            temp.ID = Int32.Parse(sdr["ID"].ToString());
                            temp.Id_jour = Int32.Parse(sdr["Id_jour"].ToString());
                            temp.Id_client = Int32.Parse(sdr["Id_client"].ToString());
                            temp.Total = Double.Parse(sdr["Total"].ToString());
                            list.Add(temp);
                        }
                    }
                    con.Close();
                }
            }
            return list;
        }


        public List<Item> GetItems()
        {
            var list = new List<Item>();
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM Items"))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (OleDbDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            var tempItem = new Item();

                            tempItem.Code = Int32.Parse(sdr["ID"].ToString());
                            tempItem.Nom = sdr["Nom"].ToString();
                            tempItem.Prix = Decimal.Parse(sdr["Prix"].ToString());
                            //tempItem.Periode = Int32.Parse(sdr["Periode"].ToString());
                            list.Add(tempItem);
                        }
                    }
                    con.Close();
                }
            }
            return list;
        }
        public List<Commande> GetBonCommande()
        {
            var list = new List<Commande>();
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM Commandes"))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (OleDbDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            var tempCommande = new Commande();
                            tempCommande.Id = Int32.Parse(sdr["ID"].ToString());
                            tempCommande.Jour = Int32.Parse(sdr["Jours"].ToString());
                            tempCommande.Client_id = Int32.Parse(sdr["Client_Id"].ToString());
                            tempCommande.Item_id = Int32.Parse(sdr["Item_Id"].ToString());
                            tempCommande.Quantite = Int32.Parse(sdr["Quantite"].ToString());
                            tempCommande.Somme_Partielle = double.Parse(sdr["Somme_Partielle"].ToString());
                            list.Add(tempCommande);
                        }
                    }
                    con.Close();
                }
            }
            return list;
        }

        public List<Commande> GetBonCommandeClient(int  id)
        {
            var list = new List<Commande>();
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("SELECT * FROM Commandes where Client_Id =" + id))
                {
                    cmd.Connection = con;
                    con.Open();
                    using (OleDbDataReader sdr = cmd.ExecuteReader())
                    {
                        while (sdr.Read())
                        {
                            var tempCommande = new Commande();
                            tempCommande.Id = Int32.Parse(sdr["ID"].ToString());
                            tempCommande.Jour = Int32.Parse(sdr["Jours"].ToString());
                            tempCommande.Client_id = Int32.Parse(sdr["Client_Id"].ToString());
                            tempCommande.Item_id = Int32.Parse(sdr["Item_Id"].ToString());
                            tempCommande.Quantite = Int32.Parse(sdr["Quantite"].ToString());
                            tempCommande.Somme_Partielle = double.Parse(sdr["Somme_Partielle"].ToString());
                            list.Add(tempCommande);
                        }
                    }
                    con.Close();
                }
            }
            return list;
        }

        public void InsertDept(Dept dept)
        {

            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("insert into Dept (Id_jour,Id_client, Total) values ('" + dept.Id_jour + "', '" + dept.Id_client + "', '"+dept.Total + "' )"))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

        }
        public void InsertClient(Client client)
        {
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("insert into Clients (Nom,Prenom,Heure,Tel) values ('" + client.Nom + "', '" + client.Prenom + "','" +client.heure+"','"+ client.tel + "')"))

                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

        }
        public void InsertItem(Item item)
        {
            
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("insert into Items (Nom,Prix) values ('" + item.Nom + "', '" + item.Prix + "' )"))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

        }
        public void InsertCommande(Commande commande)
        {

            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("insert into Commandes (Jours,Client_Id,Item_Id, Quantite,Somme_Partielle) values (" + commande.Jour + ", "+commande.Client_id+", "+commande.Item_id+", "+commande.Quantite+", "+commande.Somme_Partielle+")"))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

        }

        //Suppression d'un client a partir d'un Id recu en parametre
        public void SupprimerClient(int id)
        {
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("delete from Clients where ID = "+id))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }

        //Supprimer un item a partir d'un nom d'item
        public void SupprimerItem(int id)
        {
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("delete from Items where ID ="+id))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close(); 
                }
            }
        }
        public void SupprimerCommande(int id)
        {
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("Delete from Commandes where ID=" + id))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
        }
        public double getTotalCommandePourJour(int jour, int client_id)
        {
            return GetBonCommandeClient(client_id).Where(x => x.Jour == jour).Sum(x => x.Somme_Partielle);
        }
        public void updateCommande(List<Commande> list)
        {
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            foreach(var item in list) { 
            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("Update Commandes set Quantite="+item.Quantite+" where ID=" + item.Id))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }
            }
        }
        public void updateClient(Client client)
        {
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";
            
                using (OleDbConnection con = new OleDbConnection(constr))
                {
                    using (OleDbCommand cmd = new OleDbCommand("Update Clients set Nom='" + client.Nom + "', Prenom='"+client.Prenom+"', Tel='"+client.tel+"', Heure='"+client.heure+"' where ID=" + client.Code))
                    {
                        cmd.CommandType = System.Data.CommandType.Text;
                        cmd.Connection = con;
                        con.Open();
                        cmd.ExecuteNonQuery();
                        con.Close();
                    }
                }
            
        }
        public void updateItem(Item item)
        {
            string constr = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source =DB.accdb";

            using (OleDbConnection con = new OleDbConnection(constr))
            {
                using (OleDbCommand cmd = new OleDbCommand("Update Items set Nom='" + item.Nom + "', prix='" + item.Prix + "' where ID=" + item.Code))
                {
                    cmd.CommandType = System.Data.CommandType.Text;
                    cmd.Connection = con;
                    con.Open();
                    cmd.ExecuteNonQuery();
                    con.Close();
                }
            }

        }

    }
}
